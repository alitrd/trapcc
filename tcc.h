/*
 * TCC - TRD Configuration Compiler
 * Author: Jan de Cuveland <cuveland@kip.uni-heidelberg.de>
 *         Kirchhoff-Institut fuer Physik
 * Date: December 15, 2003
 */

// #ifndef STDINC
// #define STDINC "/cad/tools/bin/config.inc"
// #endif
//
#ifndef M4INC
#define M4INC "/cad/tools/bin/m4inc.asm"
#endif

/* Function type.  */
typedef int (*func_t) (int);

/* State type for symbols */
// typedef enum { stUNSET, stSET, stCONST, stASM, stDEF } symstate;

/* flags for symbols */
#define flDEF 0x01  /* variable was defined as command line argument */
#define flASM 0x02  /* constant is to be included in assember output */

/* Data type for links in the chain of symbols.  */
struct symrec
{
  char *name;  /* name of symbol */
  int type;    /* type of symbol: either UNSET, VAR, CONST or FNCT */
  int flags;   /* flags - see definition of flDEF, flASM above */
  union
  {
    int     var;       /* value of a VAR or CONST */
    func_t  fnctptr;   /* value of a FNCT */
    char*   str;       /* value of a string define from the command line */
  } value;
  struct symrec *next;  /* link field */
};

typedef struct symrec symrec;

/* The symbol table: a chain of `struct symrec'.  */
extern symrec *sym_table;

symrec *putsym (char const *, int);
symrec *getsym (char const *);

typedef enum { eDEBUG=0, eINFO=1, eWARN=2, eERROR=3 } loglevel_t;
void message(loglevel_t l, char* msg, ...);

void write_word(int, int, int, int);
void assemble_file(int, char *);

extern int active;
extern FILE* depfile;
extern FILE* datfile;
