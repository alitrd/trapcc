#def SML0=0x0A00;     SSSS RRRR RR-- ---- -itt tttt tttt tttt # GSM level 0 time and ignore 
#def SML1=0x0A01;     SSSS RRRR RR-- ---- -itt tttt tttt tttt # GSM level 1 time and ignore 
#def SML2=0x0A02;     SSSS RRRR RR-- ---- -itt tttt tttt tttt # GSM level 2 time and ignore 
#def SMMODE=0x0A03;   SSSS RRRR RR-- ---- pppp snme eeee dddd # GSM mode bits 
#def SMCMD=0x0A04;    ---- ---- ---- ---- cccc cccc cccc cccc # GSM command register 
#def CPU0CLK=0x0A20;  ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the CPU 0 clock 
#def CPU0SS=0x0A21;   ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of CPU 0 
#def CPU1CLK=0x0A22;  ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the CPU 1 clock 
#def CPU1SS=0x0A23;   ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of CPU 1 
#def CPU2CLK=0x0A24;  ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the CPU 2 clock 
#def CPU2SS=0x0A25;   ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of CPU 2 
#def CPU3CLK=0x0A26;  ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the CPU 3 clock 
#def CPU3SS=0x0A27;   ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of CPU 3 
#def NICLK=0x0A28;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the NI clock 
#def NICLKSS=0x0A29;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of NI 
#def FILCLK=0x0A2A;   ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the Filter clock 
#def FILCLKSS=0x0A2B; ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of Filter 
#def PRECLK=0x0A2C;   ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the Preprocessor clock 
#def PRECLKSS=0x0A2D; ---- ---- ---- ---- ---- ---- ---- ---c # Switch the clock of Preprocessor 
#def ADCEN=0x0A2E;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the ADC Enable 
#def ADCENSS=0x0A2F;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the enable of the ADCs 
#def NIODE=0x0A30;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the NI output data port 
#def NIODESS=0x0A31;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the NI output data port enable 
#def NIOCE=0x0A32;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the NI output control port 
#def NIOCESS=0x0A33;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the NI output control port enable 
#def NIIDE=0x0A34;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the NI input data ports 
#def NIIDESS=0x0A35;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the NI input data ports enable 
#def NIICE=0x0A36;    ---- ---- ---- ---- ---- ---- --St oamm # Configuration of the NI input control ports 
#def NIICESS=0x0A37;  ---- ---- ---- ---- ---- ---- ---- ---c # Switch the NI input control ports enable

#def CTGDINI=0x0B80; dddd dddd dddd dddd dddd dddd dddd dddd Global Counter/timer init data
#def CTGCTRL=0x0B81; ---- ---- ---- ---- ---S idce essb bbbb Global Counter/timer control/status
#def CTGDOUT=0x0B82; DDDD DDDD DDDD DDDD DDDD DDDD DDDD DDDD # Global Counter/timer output data
#def CTPDINI=0x0200; dddd dddd dddd dddd dddd dddd dddd dddd # Privat Counter/timer init data
#def CTPCTRL=0x0201; ---- ---- ---- ---- ---S idce essb bbbb # Privat Counter/timer control/status
#def CTPDOUT=0x0202; DDDD DDDD DDDD DDDD DDDD DDDD DDDD DDDD # Privat Counter/timer output data

#def PASADEL=0x3158;  ---- ---- ---- ---- ---- ---- aaaa aaaa # PASA delay 
#def PASAPHA=0x3159;  ---- ---- ---- ---- ---- ---- --aa aaaa # PASA phase
#def PASAPRA=0x315A;  ---- ---- ---- ---- ---- ---- --aa aaaa # Pretrigger advance 
#def PASADAC=0x315B;  ---- ---- ---- ---- ---- ---- aaaa aaaa # PASA DAC for the amplitude of the test pulse 
#def PASACHM=0x315C;  ---- ---- ---- -aaa aaaa aaaa aaaa aaab # PASA test pulse channel mask 
#def PASASTL=0x315D;  ---- ---- ---- ---- ---- ---- aaaa aaaa # PASA strobe length 
#def PASAPR1=0x315E;  ---- ---- ---- ---- ---- ---- ---- ---a # Program PASA serially and start 
#def PASAPR0=0x315F;  ---- ---- ---- ---- ---- ---- ---- ---a # Start PASA without programming serially 
#def ADCMSK=0x3050;   ---- ---- ---a aaaa aaaa aaaa aaaa aaaa # ADC Mask 
#def ADCINB=0x3051;   ---- ---- ---- ---- ---- ---- ---- --mo # ADC Invert Bits 
#def ADCDAC=0x3052;   ---- ---- ---- ---- ---- ---- ---d dddd # ADC DAC 
#def ADCPAR=0x3053;   ---- ---- ---- --ii iiss ssbz hhhe appp # ADC parameters 
#def ADCTST=0x3054;   ---- ---- ---- ---- ---- ---- ---- --tt # ADC test mode 
#def SADCAZ=0x3055;   ---- ---- ---- ---- ---- ---- ---- ---a # Slow control ADC auto zero 
#def SADCTRG=0x3161;  ---- ---- ---- ---- ---- ---- ---- ---a # Slow ADC channel trigger 
#def SADCRUN=0x3162;  ---- ---- ---- ---- ---- ---- ---- ---a # Slow ADC channel run 
#def SADCPWR=0x3163;  ---- ---- ---- ---- ---- ---- ---- -aaa # Slow ADC power 
#def SADCSTA=0x3164;  ---- ---- ---- ---- ---- ---- -ret aiii # Slow ADC channel status read only 
#def L0TSIM=0x3165;   ---- ---- ---- ---- --aa aaaa aaaa aaaa # L0 time for simulation 
#def SADCEC=0x3166;   ---- ---- ---- ---- ---- ---- -daa ates # Slow ADC channel external control (not in trap2) 
#def SADCC0=0x3168;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 0
#def SADCC1=0x3169;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 1
#def SADCC2=0x316A;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 2
#def SADCC3=0x316B;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 3
#def SADCC4=0x316C;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 4
#def SADCC5=0x316D;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 5
#def SADCC6=0x316E;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 6
#def SADCC7=0x316F;   ---- ---- ---- ---- ---- --AA AAAA AAAA # Slow ADC channel 7
#def SADCMC=0x3170;   ---- ---- ---- ---- ---- ---- aaaa aaaa # Slow ADC measurement cycles 
#def SADCOC=0x3171;   ---- ---- ---- ---- ---- ---- aaaa aaaa # Slow ADC o峴et measurement cycles 
#def SADCGTB=0x3172;  hhhh gggg ffff eeee dddd cccc bbbb aaaa # Slow ADC gain table 
#def SADCTC=0x3173;   ---- ---- ---- ---- ---- ---- ---- -aaa # Slow ADC temperature measurement cycles 
#def ADCCPU=0x0100;   ---- ---- ---- ---- ---- -eaa aaaa aaaa # Direct ADC to CPU interface 
#def SEBDEN=0x3178;   ---- ---- ---- ---- ---- ---- ---- -eee # Single ended bidir output enables 
#def SEBDOU=0x3179;   ---- ---- ---- ---- ---- ---- ---- -ooo # Single ended bidir outputs 
#def SEBDIN=0x317A;   ---- ---- ---- ---- ---- ---- ---- -iii # Single ended bidir inputs (read only) 
#def CHIPID=0x3160;   ---- ---- ---- --AA AAAA AAAA AAAA AAAA # Chip ID read only 
#def TPPT0=0x3000;    ---- ---- ---- ---- ---- ---- -ddd dddd # Common Timer Delay 
#def TPPAE=0x3004;    ---- ---- ---- ---- ---- ---- -ddd dddd # Event Bu容r DataAcquisition End 
#def TPPGR=0x3003;    ---- ---- ---- ---- ---- ---- -ddd dddd # Response Time to GSM 
#def FLBY=0x3018;     ---- ---- ---- ---- ---- ---- ---- ---b # Nonlinearity Correction Filter Bypass 
#def FLL=0x3100;      ---- ---- ---- ---- ---- ---- --dd dddd # Nonlinearity Correction Filter Look Up Table
#def FPBY=0x3019;     ---- ---- ---- ---- ---- ---- ---- ---b # Pedestal Correction Filter Bypass 
#def FPTC=0x3020;     ---- ---- ---- ---- ---- ---- ---- --dd # Pedestal Correction Time Constant 
#def FPNP=0x3021;     ---- ---- ---- ---- ---- ---d dddd dddd # Pedestal Correction Additive 
#def FPCL=0x3022;     ---- ---- ---- ---- ---- ---- ---- ---b # Pedestal Correction Filter Clear
#def FPA=0x3060;      --dd dddd dddd dddd dddd dddd dddd dddd # Pedestal Correction Filter Accumulators 
#def FGBY=0x301A;     ---- ---- ---- ---- ---- ---- ---- ---b # Gain Correction Filter Bypass 
#def FGFn=0x3080;     ---- ---- ---- ---- ---- ---d dddd dddd # Gain Correction Filter Factors 
#def FGAn=0x30A0;     ---- ---- ---- ---- ---- ---- --dd dddd # Gain Correction Filter Additive 
#def FGTA=0x3028;     ---- ---- ---- ---- ---- dddd dddd dddd # Gain Correction Counter Threshold A 
#def FGTB=0x3029;     ---- ---- ---- ---- ---- dddd dddd dddd # Gain Correction Counter Threshold B 
#def FGCL=0x302A;     ---- ---- ---- ---- ---- ---- ---- ---b # Gain Correction Filter Clear 
#def FGCAn=0x30C0;    ---- --dd dddd dddd dddd dddd dddd dddd # Gain Correction Filter Counter A 
#def FGCBn=0x30C0;    ---- --dd dddd dddd dddd dddd dddd dddd # Gain Correction Filter Counter B 
#def FTBY=0x301B;     ---- ---- ---- ---- ---- ---- ---- ---b # Tail Cancellation Filter Bypass 
#def FTAL=0x3030;     ---- ---- ---- ---- ---- --dd dddd dddd # Tail Cancellation Long Decay Weight 
#def FTLL=0x3031;     ---- ---- ---- ---- ---- --dd dddd dddd # Tail Cancellation Long Decay Parameter 
#def FTLS=0x3032;     ---- ---- ---- ---- ---- --dd dddd dddd # Tail Cancellation Short Decay Parameter 
#def FCBY=0x301C;     ---- ---- ---- ---- ---- ---- ---- ---b # Crosstalk Cancellation Filter Bypass 
#def FCWn=0x3038;     ---- ---- ---- ---- ---- ---- dddd dddd # Crosstalk Cancellation Filter Weights 
#def TPFS=0x3001;     ---- ---- ---- ---- ---- ---- -ddd dddd # Preprocessor s Linear Fit Start 
#def TPFE=0x3002;     ---- ---- ---- ---- ---- ---- -ddd dddd # Preprocessor s Linear Fit End 
#def TPQS0=0x3005;    ---- ---- ---- ---- ---- ---- -ddd dddd # Charge Accumulator 0 Start 
#def TPQE0=0x3006;    ---- ---- ---- ---- ---- ---- -ddd dddd # Charge Accumulator 0 End 
#def TPQS1=0x3007;    ---- ---- ---- ---- ---- ---- -ddd dddd # Charge Accumulator 1 Start 
#def TPQE1=0x3008;    ---- ---- ---- ---- ---- ---- -ddd dddd # Charge Accumulator 1 End 
#def TPHT=0x3041;     ---- ---- ---- ---- --dd dddd dddd dddd # Cluster Charge Threshold 
#def TPVBY=0x3043;    ---- ---- ---- ---- ---- ---- ---- ---b # Cluster Verification Bypass 
#def TPVT=0x3042;     ---- ---- ---- ---- ---- ---- --dd dddd # Cluster Quality Threshold 
#def TPFP=0x3040;     ---- ---- ---- ---- ---- ---- --dd dddd # Filtered Pedestal 
#def TPL=0x3180;      ---- ---- ---- ---- ---- ---- ---d dddd # Position Correction Look Up Table 
#def TPCL=0x3045;     ---- ---- ---- ---- ---- ---- ---d dddd # Tracklet Candidate Left Hit Number Threshold 
#def TPCT=0x3044;     ---- ---- ---- ---- ---- ---- ---d dddd # Tracklet Candidate Total Hit Number Threshold 
#def TPD=0x3047;      ---- ---- ---- ---- ---- ---- ---- dddd # Tracklet Candidate Latch Delay 
#def TPH=0x3140;      ---- ---- ---- ---- ---- ---- ---d dddd # Number of Hits in Channel n 
#def TPCBY=0x3046;    ---- ---- ---- ---- ---- ---- ---- ---b # Test Indices Flag 
#def TPCI0=0x3048;    ---- ---- ---- ---- ---- ---- ---d dddd # Test Index for CPU 0
#def TPCI1=0x3049;    ---- ---- ---- ---- ---- ---- ---d dddd # Test Index for CPU 1
#def TPCI2=0x304A;    ---- ---- ---- ---- ---- ---- ---d dddd # Test Index for CPU 2
#def TPCI3=0x304B;    ---- ---- ---- ---- ---- ---- ---d dddd # Test Index for CPU 3
#def EBD=0x3009;      ---- ---- ---- ---- ---- ---- ---- -ddd # Event Bu容r Input Delay
#def EBSF=0x300C;     ---- ---- ---- ---- ---- ---- ---- ---b # Event Bu容r Storage Mode 
#def EBAQA=0x300A;    ---- ---- ---- ---- ---- ---- -ddd dddd # Event Bu容r Acquisition O峴et Address 
#def EBSIM=0x300D;    ---- ---- ---- ---- ---- ---- ---- ---b # Event Bu容r Simulation Mode 
#def EBSIA=0x300B;    ---- ---- ---- ---- ---- ---- -ddd dddd # Event Bu容r Simulation O峴et Address
#def EBR=0x0800;      ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) 
#def EBR0=0x0800;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 0
#def EBR1=0x0840;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 1
#def EBR2=0x0880;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 2
#def EBR3=0x08C0;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 3
#def EBR4=0x0900;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 4
#def EBR5=0x0940;     ---- ---- ---- ---- ---- -pdd dddd dddd # Event Bu容r Data Read (LIO) channel 5
#def EBW=0x2000;      ---- ---- ---- ---- ---- --dd dddd dddd # Event Bu容r Data Write 
#def EBPP=0x300E;     ---- ---- ---- ---- ---- ---- ---- ---b # Event Bu容r Parity Bit Mask 
#def EBPC=0x300F;     ---- ---- ---- ---- ---- ---- ---- ---b # Event Bu容r Parity Violation Counters Clear 
#def EBP0=0x3010;     ---- ---- ---- ---- ---- ---d dddd dddd # Event Bu容r Parity Violation Counter 0
#def EBP1=0x3011;     ---- ---- ---- ---- ---- ---d dddd dddd # Event Bu容r Parity Violation Counter 1
#def EBP2=0x3012;     ---- ---- ---- ---- ---- ---d dddd dddd # Event Bu容r Parity Violation Counter 2
#def EBP3=0x3013;     ---- ---- ---- ---- ---- ---d dddd dddd # Event Bu容r Parity Violation Counter 3
#def EBIS=0x3014;     ---- ---- ---- ---- ---- --dd dddd dddd # Event Bu容r Single Indicator Threshold 
#def EBIT=0x3015;     ---- ---- ---- ---- ---- dddd dddd dddd # Event Bu容r SumIndicator Threshold 
#def EBIL=0x3016;     ---- ---- ---- ---- ---- ---- dddd dddd # Event Bu容r Indicator Look-up Table 
#def EBIN=0x3017;     ---- ---- ---- ---- ---- ---- ---- ---b # Event Bu容r Indicator Neighbor Sensitivity 
#def EBI=0x0980;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI0=0x0980;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI1=0x0981;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI2=0x0982;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI3=0x0983;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI4=0x0984;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI5=0x0985;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI6=0x0986;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI7=0x0987;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI8=0x0988;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBI9=0x0989;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBIA=0x098A;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def EBIB=0x098B;      dddd dddd dddd dddd dddd dddd dddd dddd # Event Bu容r Indicators (LIO) 
#def ARBTIM=0x0A3F;   ---- ---- ---- ---- ---- ---- ---- wwrr # Configuration of the timing on the global bus 
#def MEMRW=0xD000;    ---- ---- ---- ---- ---- ---- -www wrri # Configuration Global bus access to IMEM/DMEM 
#def MEMCOR=0xD001;   ---- ---- ---- ---- ---- ---b dddd iiii # Configuration hamming correction IMEM/DMEM 
#def DMDELA=0xD002;   ---- ---- ---- ---- ---- ---- ---- aaaa # Configuration internal timing in DMEM address lines 
#def DMDELS=0xD003;   ---- ---- ---- ---- ---- ---- ---- ssss # Configuration internal timing in DMEM sense amplifiers 
#def HCNTI0=0xD010;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters IMEM0 
#def HCNTI1=0xD011;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters IMEM1 
#def HCNTI2=0xD012;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters IMEM2 
#def HCNTI3=0xD013;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters IMEM3 
#def HCNTD0=0xD014;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters DMEM0 
#def HCNTD1=0xD015;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters DMEM1 
#def HCNTD2=0xD016;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters DMEM2 
#def HCNTD3=0xD017;   O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN # Hamming counters DMEM3 
#def IA0=0x0B00;      ---- ---- ---- ---- ---- aaaa aaaa aaaa # Interrupt address CPU 0
#def IA1=0x0B20;      ---- ---- ---- ---- ---- aaaa aaaa aaaa # Interrupt address CPU 1
#def IA2=0x0B40;      ---- ---- ---- ---- ---- aaaa aaaa aaaa # Interrupt address CPU 2
#def IA3=0x0B60;      ---- ---- ---- ---- ---- aaaa aaaa aaaa # Interrupt address CPU 3
#def IRQSW0=0x0B0D;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask software interrupts CPU 0
#def IRQSW1=0x0B2D;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask software interrupts CPU 1
#def IRQSW2=0x0B4D;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask software interrupts CPU 2
#def IRQSW3=0x0B6D;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask software interrupts CPU 3
#def IRQHW0=0x0B0E;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask hardware interrupts CPU 0
#def IRQHW1=0x0B2E;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask hardware interrupts CPU 1
#def IRQHW2=0x0B4E;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask hardware interrupts CPU 2
#def IRQHW3=0x0B6E;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask hardware interrupts CPU 3
#def IRQHL0=0x0B0F;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask high-level interrupts CPU 0
#def IRQHL1=0x0B2F;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask high-level interrupts CPU 1
#def IRQHL2=0x0B4F;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask high-level interrupts CPU 2
#def IRQHL3=0x0B6F;   ---- ---- ---- ---- ---m mmmm mmmm mmmm # Mask high-level interrupts CPU 3
#def NMOD=0x0D40;     ---- ---- ---- ---- ---- ---- ---i cmmm # NI network mode control 
#def NTRO=0x0D43;     ---- ---- ---- --ii iddd cccb bbaa afff # NI trigger readout order 
#def NES=0x0D45;      rrrr rrrr rrrr rrrr tttt tttt tttt tttt # NI end signatures 
#def NCUT=0x0D4C;     dddd dddd cccc cccc bbbb bbbb aaaa aaaa # NI cut trigger stream 
#def NRRO=0x0D44;     ---- ---- ---- --ii iddd cccb bbaa afff # NI raw data readout order 
#def NTP=0x0D46;      pppp pppp pppp pppp pppp pppp pppp pppp # NI test pattern 
#def NP0=0x0D48;      ---- ---- ---- ---- ---- -ppp pfff fecs # NI input port 0 control 
#def NP1=0x0D49;      ---- ---- ---- ---- ---- -ppp pfff fecs # NI input port 1 control 
#def NP2=0x0D4A;      ---- ---- ---- ---- ---- -ppp pfff fecs # NI input port 2 control 
#def NP3=0x0D4B;      ---- ---- ---- ---- ---- -ppp pfff fecs # NI input port 3 control 
#def NLP=0x00C1;      ---- ---- HHHH HHHH LLLL LLLL CCCC CCCC # NI (LIO) parity and word counter 
#def NED=0x0D42;      ---- ---- ---- ---- orpp ppff ffcc csss # NI output excludes and ctrl delay 
#def NDLY=0x0D41;     --jj jiii hhhg ggff feee dddc ccbb baaa # NI data delays
#def NBND=0x0D47;     ---- ---- ---- ---- hhhh hhhh llll llll # NI fifo bounds 
#def NLF=0x00C0;      ---- ---- ---- ---- ---- -DSS EHLZ YXWV # NI (LIO) FIFO states 
#def NLE=0x00C2;      ---- ---- ---- ---- ---- ---- EEEE EEEE # NI (LIO) FIFO entries 
#def NFE=0x0DC1;      ---- ---- ---- ---- ---- ---- ---- DCBA # NI fifo errors 
#def NCTRL=0x0DC0;    ---- ---- ---- ---- ---- ---- ---- ---C # NI control input 
#def NFSM=0x0DC2;     ---- ---- ---- ---- ---- ---- ---S SSSS # NI FSM state
#def NITM0=0x0A08;    ---- ---- ---- ---- --tt tttt tttt tttt # NI timer 0
#def NITM1=0x0A09;    ---- ---- ---- ---- --tt tttt tttt tttt # NI timer 1
#def NITM2=0x0A0A;    ---- ---- ---- ---- --tt tttt tttt tttt # NI timer 2
#def NIP4D=0x0A0B;    ---- ---- ---- ---- --tt tttt tttt tttt # NI port 4 delay
#def SMOFFON=0x0A05;  ---- ---- dddd dddd dddd dddd dddd dddd # GSM command register for switching clock/enable, W/O
#def SMON=0x0A06;     ---- ---- ---- ---- ---- dddd dddd dddd # GSM command register for switching on clock/enable, W/O
#def SMOFF=0x0A07;    ---- ---- ---- ---- ---- dddd dddd dddd # GSM command register for switching off clock/enable, W/O
#def NODP=0x0000;     dddd dddd dddd dddd dddd dddd dddd dddd # NI output data port (LIO), W/O
#def CMD_LP=0x0012;       ---- ---- ---- ---- ---- ---- ---- ---- # go to low power state
#def CMD_ACQ=0x0112;      ---- ---- ---- ---- ---- ---- ---- ---- # go to acquisition state
#def CMD_CHK_TST=0x0212;  ---- ---- ---- ---- ---- ---- ---- ---- # go to test mode
#def CMD_EXT_CLR=0x0312;  ---- ---- ---- ---- ---- ---- ---- ---- # exit clear state
#def CMD_CLEAR=0x0412;    ---- ---- ---- ---- ---- ---- ---- ---- # go to clear state
#def CMD_PRETRIGG=0x0512; ---- ---- ---- ---- ---- ---- ---- ---- # internally generate pretrigger
#def CMD_SELFTP=0x0612;   ---- ---- ---- ---- ---- ---- ---- ---- # start the self-test state machine
#def CMD_CPU_DONE=0x0712; ---- ---- ---- ---- ---- ---- ---- ---- # CPU finished the tracklet processing or raw data compression
#def GBUSR0=0x0300; -- readonly
#def GBUSR1=0x0301; -- readonly

#def C08CPU0 = 0xC00;
#def C09CPU0 = 0xC01;
#def C10CPU0 = 0xC02;
#def C11CPU0 = 0xC03;

#def C08CPU1 = 0xC08;
#def C09CPU1 = 0xC09;
#def C10CPU1 = 0xC0A;
#def C11CPU1 = 0xC0B;

#def C08CPU2 = 0xC10;
#def C09CPU2 = 0xC11;
#def C10CPU2 = 0xC12;
#def C11CPU2 = 0xC13;

#def C08CPU3 = 0xC18;
#def C09CPU3 = 0xC19;
#def C10CPU3 = 0xC1A;
#def C11CPU3 = 0xC1B;

#def C12CPUA = 0xC04;
#def C13CPUA = 0xC05;
#def C14CPUA = 0xC06;
#def C15CPUA = 0xC07;
