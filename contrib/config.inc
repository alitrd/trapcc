/* GSM level 0 time and ignore  */
const SML0     = 0x0A00;
/* SSSS RRRR RR-- ---- -itt tttt tttt tttt */

/* GSM level 1 time and ignore  */
const SML1     = 0x0A01;
/* SSSS RRRR RR-- ---- -itt tttt tttt tttt */

/* GSM level 2 time and ignore  */
const SML2     = 0x0A02;
/* SSSS RRRR RR-- ---- -itt tttt tttt tttt */

/* GSM mode bits  */
const SMMODE   = 0x0A03;
/* SSSS RRRR RR-- ---- pppp snme eeee dddd */

/* GSM command register  */
const SMCMD    = 0x0A04;
/* ---- ---- ---- ---- cccc cccc cccc cccc */

/* Configuration of the CPU 0 clock  */
const CPU0CLK  = 0x0A20;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of CPU 0  */
const CPU0SS   = 0x0A21;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the CPU 1 clock  */
const CPU1CLK  = 0x0A22;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of CPU 1  */
const CPU1SS   = 0x0A23;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the CPU 2 clock  */
const CPU2CLK  = 0x0A24;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of CPU 2  */
const CPU2SS   = 0x0A25;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the CPU 3 clock  */
const CPU3CLK  = 0x0A26;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of CPU 3  */
const CPU3SS   = 0x0A27;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the NI clock  */
const NICLK    = 0x0A28;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of NI  */
const NICLKSS  = 0x0A29;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the Filter clock  */
const FILCLK   = 0x0A2A;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of Filter  */
const FILCLKSS = 0x0A2B;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the Preprocessor clock  */
const PRECLK   = 0x0A2C;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the clock of Preprocessor  */
const PRECLKSS = 0x0A2D;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the ADC Enable  */
const ADCEN    = 0x0A2E;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the enable of the ADCs  */
const ADCENSS  = 0x0A2F;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the NI output data port  */
const NIODE    = 0x0A30;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the NI output data port enable  */
const NIODESS  = 0x0A31;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the NI output control port  */
const NIOCE    = 0x0A32;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the NI output control port enable  */
const NIOCESS  = 0x0A33;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the NI input data ports  */
const NIIDE    = 0x0A34;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the NI input data ports enable  */
const NIIDESS  = 0x0A35;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* Configuration of the NI input control ports  */
const NIICE    = 0x0A36;
/* ---- ---- ---- ---- ---- ---- --St oamm */

/* Switch the NI input control ports enable  */
const NIICESS  = 0x0A37;
/* ---- ---- ---- ---- ---- ---- ---- ---c */

/* PASA delay  */
const PASADEL  = 0x3158;
/* ---- ---- ---- ---- ---- ---- aaaa aaaa */

/* PASA phase */
const PASAPHA  = 0x3159;
/* ---- ---- ---- ---- ---- ---- --aa aaaa */

/* Pretrigger advance  */
const PASAPRA  = 0x315A;
/* ---- ---- ---- ---- ---- ---- --aa aaaa */

/* PASA DAC for the amplitude of the test pulse  */
const PASADAC  = 0x315B;
/* ---- ---- ---- ---- ---- ---- aaaa aaaa */

/* PASA test pulse channel mask  */
const PASACHM  = 0x315C;
/* ---- ---- ---- -aaa aaaa aaaa aaaa aaab */

/* PASA strobe length  */
const PASASTL  = 0x315D;
/* ---- ---- ---- ---- ---- ---- aaaa aaaa */

/* Program PASA serially and start  */
const PASAPR1  = 0x315E;
/* ---- ---- ---- ---- ---- ---- ---- ---a */

/* Start PASA without programming serially  */
const PASAPR0  = 0x315F;
/* ---- ---- ---- ---- ---- ---- ---- ---a */

/* ADC Mask  */
const ADCMSK   = 0x3050;
/* ---- ---- ---a aaaa aaaa aaaa aaaa aaaa */

/* ADC Invert Bits  */
const ADCINB   = 0x3051;
/* ---- ---- ---- ---- ---- ---- ---- --mo */

/* ADC DAC  */
const ADCDAC   = 0x3052;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* ADC parameters  */
const ADCPAR   = 0x3053;
/* ---- ---- ---- --ii iiss ssbz hhhe appp */

/* ADC test mode  */
const ADCTST   = 0x3054;
/* ---- ---- ---- ---- ---- ---- ---- --tt */

/* Slow control ADC auto zero  */
const SADCAZ   = 0x3055;
/* ---- ---- ---- ---- ---- ---- ---- ---a */

/* Slow ADC channel trigger  */
const SADCTRG  = 0x3161;
/* ---- ---- ---- ---- ---- ---- ---- ---a */

/* Slow ADC channel run  */
const SADCRUN  = 0x3162;
/* ---- ---- ---- ---- ---- ---- ---- ---a */

/* Slow ADC power  */
const SADCPWR  = 0x3163;
/* ---- ---- ---- ---- ---- ---- ---- -aaa */

/* Slow ADC channel status read only  */
const SADCSTA  = 0x3164;
/* ---- ---- ---- ---- ---- ---- -ret aiii */

/* L0 time for simulation  */
const L0TSIM   = 0x3165;
/* ---- ---- ---- ---- --aa aaaa aaaa aaaa */

/* Slow ADC channel external control (not in trap2)  */
const SADCEC   = 0x3166;
/* ---- ---- ---- ---- ---- ---- -daa ates */

/* Slow ADC channel 0 */
const SADCC0   = 0x3168;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 1 */
const SADCC1   = 0x3169;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 2 */
const SADCC2   = 0x316A;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 3 */
const SADCC3   = 0x316B;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 4 */
const SADCC4   = 0x316C;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 5 */
const SADCC5   = 0x316D;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 6 */
const SADCC6   = 0x316E;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC channel 7 */
const SADCC7   = 0x316F;
/* ---- ---- ---- ---- ---- --AA AAAA AAAA */

/* Slow ADC measurement cycles  */
const SADCMC   = 0x3170;
/* ---- ---- ---- ---- ---- ---- aaaa aaaa */

/* Slow ADC o峴et measurement cycles  */
const SADCOC   = 0x3171;
/* ---- ---- ---- ---- ---- ---- aaaa aaaa */

/* Slow ADC gain table  */
const SADCGTB  = 0x3172;
/* hhhh gggg ffff eeee dddd cccc bbbb aaaa */

/* Slow ADC temperature measurement cycles  */
const SADCTC   = 0x3173;
/* ---- ---- ---- ---- ---- ---- ---- -aaa */

/* Direct ADC to CPU interface  */
const ADCCPU   = 0x0100;
/* ---- ---- ---- ---- ---- -eaa aaaa aaaa */

/* Single ended bidir output enables  */
const SEBDEN   = 0x3178;
/* ---- ---- ---- ---- ---- ---- ---- -eee */

/* Single ended bidir outputs  */
const SEBDOU   = 0x3179;
/* ---- ---- ---- ---- ---- ---- ---- -ooo */

/* Single ended bidir inputs (read only)  */
const SEBDIN   = 0x317A;
/* ---- ---- ---- ---- ---- ---- ---- -iii */

/* Chip ID read only  */
const CHIPID   = 0x3160;
/* ---- ---- ---- --AA AAAA AAAA AAAA AAAA */

/* Common Timer Delay  */
const TPPT0    = 0x3000;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Event Bu容r DataAcquisition End  */
const TPPAE    = 0x3004;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Response Time to GSM  */
const TPPGR    = 0x3003;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Nonlinearity Correction Filter Bypass  */
const FLBY     = 0x3018;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Nonlinearity Correction Filter Look Up Table */
const FLL      = 0x3100;
/* ---- ---- ---- ---- ---- ---- --dd dddd */

/* Pedestal Correction Filter Bypass  */
const FPBY     = 0x3019;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Pedestal Correction Time Constant  */
const FPTC     = 0x3020;
/* ---- ---- ---- ---- ---- ---- ---- --dd */

/* Pedestal Correction Additive  */
const FPNP     = 0x3021;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Pedestal Correction Filter Clear */
const FPCL     = 0x3022;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Pedestal Correction Filter Accumulators  */
const FPA      = 0x3060;
/* --dd dddd dddd dddd dddd dddd dddd dddd */

/* Gain Correction Filter Bypass  */
const FGBY     = 0x301A;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Gain Correction Filter Factors  */
const FGFn     = 0x3080;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Gain Correction Filter Additive  */
const FGAn     = 0x30A0;
/* ---- ---- ---- ---- ---- ---- --dd dddd */

/* Gain Correction Counter Threshold A  */
const FGTA     = 0x3028;
/* ---- ---- ---- ---- ---- dddd dddd dddd */

/* Gain Correction Counter Threshold B  */
const FGTB     = 0x3029;
/* ---- ---- ---- ---- ---- dddd dddd dddd */

/* Gain Correction Filter Clear  */
const FGCL     = 0x302A;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Gain Correction Filter Counter A  */
const FGCAn    = 0x30C0;
/* ---- --dd dddd dddd dddd dddd dddd dddd */

/* Gain Correction Filter Counter B  */
const FGCBn    = 0x30C0;
/* ---- --dd dddd dddd dddd dddd dddd dddd */

/* Tail Cancellation Filter Bypass  */
const FTBY     = 0x301B;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Tail Cancellation Long Decay Weight  */
const FTAL     = 0x3030;
/* ---- ---- ---- ---- ---- --dd dddd dddd */

/* Tail Cancellation Long Decay Parameter  */
const FTLL     = 0x3031;
/* ---- ---- ---- ---- ---- --dd dddd dddd */

/* Tail Cancellation Short Decay Parameter  */
const FTLS     = 0x3032;
/* ---- ---- ---- ---- ---- --dd dddd dddd */

/* Crosstalk Cancellation Filter Bypass  */
const FCBY     = 0x301C;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Crosstalk Cancellation Filter Weights  */
const FCWn     = 0x3038;
/* ---- ---- ---- ---- ---- ---- dddd dddd */

/* Preprocessor s Linear Fit Start  */
const TPFS     = 0x3001;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Preprocessor s Linear Fit End  */
const TPFE     = 0x3002;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Charge Accumulator 0 Start  */
const TPQS0    = 0x3005;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Charge Accumulator 0 End  */
const TPQE0    = 0x3006;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Charge Accumulator 1 Start  */
const TPQS1    = 0x3007;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Charge Accumulator 1 End  */
const TPQE1    = 0x3008;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Cluster Charge Threshold  */
const TPHT     = 0x3041;
/* ---- ---- ---- ---- --dd dddd dddd dddd */

/* Cluster Verification Bypass  */
const TPVBY    = 0x3043;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Cluster Quality Threshold  */
const TPVT     = 0x3042;
/* ---- ---- ---- ---- ---- ---- --dd dddd */

/* Filtered Pedestal  */
const TPFP     = 0x3040;
/* ---- ---- ---- ---- ---- ---- --dd dddd */

/* Position Correction Look Up Table  */
const TPL      = 0x3180;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Tracklet Candidate Left Hit Number Threshold  */
const TPCL     = 0x3045;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Tracklet Candidate Total Hit Number Threshold  */
const TPCT     = 0x3044;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Tracklet Candidate Latch Delay  */
const TPD      = 0x3047;
/* ---- ---- ---- ---- ---- ---- ---- dddd */

/* Number of Hits in Channel n  */
const TPH      = 0x3140;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Test Indices Flag  */
const TPCBY    = 0x3046;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Test Index for CPU 0 */
const TPCI0    = 0x3048;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Test Index for CPU 1 */
const TPCI1    = 0x3049;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Test Index for CPU 2 */
const TPCI2    = 0x304A;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Test Index for CPU 3 */
const TPCI3    = 0x304B;
/* ---- ---- ---- ---- ---- ---- ---d dddd */

/* Event Bu容r Input Delay */
const EBD      = 0x3009;
/* ---- ---- ---- ---- ---- ---- ---- -ddd */

/* Event Bu容r Storage Mode  */
const EBSF     = 0x300C;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Event Bu容r Acquisition O峴et Address  */
const EBAQA    = 0x300A;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Event Bu容r Simulation Mode  */
const EBSIM    = 0x300D;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Event Bu容r Simulation O峴et Address */
const EBSIA    = 0x300B;
/* ---- ---- ---- ---- ---- ---- -ddd dddd */

/* Event Bu容r Data Read (LIO)  */
const EBR      = 0x0800;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 0 */
const EBR0     = 0x0800;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 1 */
const EBR1     = 0x0840;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 2 */
const EBR2     = 0x0880;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 3 */
const EBR3     = 0x08C0;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 4 */
const EBR4     = 0x0900;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Read (LIO) channel 5 */
const EBR5     = 0x0940;
/* ---- ---- ---- ---- ---- -pdd dddd dddd */

/* Event Bu容r Data Write  */
const EBW      = 0x2000;
/* ---- ---- ---- ---- ---- --dd dddd dddd */

/* Event Bu容r Parity Bit Mask  */
const EBPP     = 0x300E;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Event Bu容r Parity Violation Counters Clear  */
const EBPC     = 0x300F;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Event Bu容r Parity Violation Counter 0 */
const EBP0     = 0x3010;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Event Bu容r Parity Violation Counter 1 */
const EBP1     = 0x3011;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Event Bu容r Parity Violation Counter 2 */
const EBP2     = 0x3012;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Event Bu容r Parity Violation Counter 3 */
const EBP3     = 0x3013;
/* ---- ---- ---- ---- ---- ---d dddd dddd */

/* Event Bu容r Single Indicator Threshold  */
const EBIS     = 0x3014;
/* ---- ---- ---- ---- ---- --dd dddd dddd */

/* Event Bu容r SumIndicator Threshold  */
const EBIT     = 0x3015;
/* ---- ---- ---- ---- ---- dddd dddd dddd */

/* Event Bu容r Indicator Look-up Table  */
const EBIL     = 0x3016;
/* ---- ---- ---- ---- ---- ---- dddd dddd */

/* Event Bu容r Indicator Neighbor Sensitivity  */
const EBIN     = 0x3017;
/* ---- ---- ---- ---- ---- ---- ---- ---b */

/* Event Bu容r Indicators (LIO)  */
const EBI      = 0x0980;
/* dddd dddd dddd dddd dddd dddd dddd dddd */

/* Configuration of the timing on the global bus  */
const ARBTIM   = 0x0A3F;
/* ---- ---- ---- ---- ---- ---- ---- wwrr */

/* Configuration Global bus access to IMEM/DMEM  */
const MEMRW    = 0xD000;
/* ---- ---- ---- ---- ---- ---- -www wrri */

/* Configuration hamming correction IMEM/DMEM  */
const MEMCOR   = 0xD001;
/* ---- ---- ---- ---- ---- ---b dddd iiii */

/* Configuration internal timing in DMEM address lines  */
const DMDELA   = 0xD002;
/* ---- ---- ---- ---- ---- ---- ---- aaaa */

/* Configuration internal timing in DMEM sense amplifiers  */
const DMDELS   = 0xD003;
/* ---- ---- ---- ---- ---- ---- ---- ssss */

/* Hamming counters IMEM0  */
const HCNTI0   = 0xD010;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters IMEM1  */
const HCNTI1   = 0xD011;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters IMEM2  */
const HCNTI2   = 0xD012;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters IMEM3  */
const HCNTI3   = 0xD013;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters DMEM0  */
const HCNTD0   = 0xD014;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters DMEM1  */
const HCNTD1   = 0xD015;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters DMEM2  */
const HCNTD2   = 0xD016;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Hamming counters DMEM3  */
const HCNTD3   = 0xD017;
/* O0CC CCCC CCCC PPPP PPPP PPNN NNNN NNNN */

/* Interrupt address CPU 0 */
const IA0      = 0x0B00;
/* ---- ---- ---- ---- ---- aaaa aaaa aaaa */

/* Interrupt address CPU 1 */
const IA1      = 0x0B20;
/* ---- ---- ---- ---- ---- aaaa aaaa aaaa */

/* Interrupt address CPU 2 */
const IA2      = 0x0B40;
/* ---- ---- ---- ---- ---- aaaa aaaa aaaa */

/* Interrupt address CPU 3 */
const IA3      = 0x0B60;
/* ---- ---- ---- ---- ---- aaaa aaaa aaaa */

/* Mask software interrupts CPU 0 */
const IRQSW0   = 0x0B0D;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask software interrupts CPU 1 */
const IRQSW1   = 0x0B2D;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask software interrupts CPU 2 */
const IRQSW2   = 0x0B4D;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask software interrupts CPU 3 */
const IRQSW3   = 0x0B6D;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask hardware interrupts CPU 0 */
const IRQHW0   = 0x0B0E;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask hardware interrupts CPU 1 */
const IRQHW1   = 0x0B2E;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask hardware interrupts CPU 2 */
const IRQHW2   = 0x0B4E;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask hardware interrupts CPU 3 */
const IRQHW3   = 0x0B6E;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask high-level interrupts CPU 0 */
const IRQHL0   = 0x0B0F;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask high-level interrupts CPU 1 */
const IRQHL1   = 0x0B2F;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask high-level interrupts CPU 2 */
const IRQHL2   = 0x0B4F;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* Mask high-level interrupts CPU 3 */
const IRQHL3   = 0x0B6F;
/* ---- ---- ---- ---- ---m mmmm mmmm mmmm */

/* NI network mode control  */
const NMOD     = 0x0D40;
/* ---- ---- ---- ---- ---- ---- ---i cmmm */

/* NI trigger readout order  */
const NTRO     = 0x0D43;
/* ---- ---- ---- --ii iddd cccb bbaa afff */

/* NI end signatures  */
const NES      = 0x0D45;
/* rrrr rrrr rrrr rrrr tttt tttt tttt tttt */

/* NI cut trigger stream  */
const NCUT     = 0x0D4C;
/* dddd dddd cccc cccc bbbb bbbb aaaa aaaa */

/* NI raw data readout order  */
const NRRO     = 0x0D44;
/* ---- ---- ---- --ii iddd cccb bbaa afff */

/* NI test pattern  */
const NTP      = 0x0D46;
/* pppp pppp pppp pppp pppp pppp pppp pppp */

/* NI input port 0 control  */
const NP0      = 0x0D48;
/* ---- ---- ---- ---- ---- -ppp pfff fecs */

/* NI input port 1 control  */
const NP1      = 0x0D49;
/* ---- ---- ---- ---- ---- -ppp pfff fecs */

/* NI input port 2 control  */
const NP2      = 0x0D4A;
/* ---- ---- ---- ---- ---- -ppp pfff fecs */

/* NI input port 3 control  */
const NP3      = 0x0D4B;
/* ---- ---- ---- ---- ---- -ppp pfff fecs */

/* NI (LIO) parity and word counter  */
const NLP      = 0x00C1;
/* ---- ---- HHHH HHHH LLLL LLLL CCCC CCCC */

/* NI output excludes and ctrl delay  */
const NED      = 0x0D42;
/* ---- ---- ---- ---- orpp ppff ffcc csss */

/* NI data delays */
const NDLY     = 0x0D41;
/* --jj jiii hhhg ggff feee dddc ccbb baaa */

/* NI fifo bounds  */
const NBND     = 0x0D47;
/* ---- ---- ---- ---- hhhh hhhh llll llll */

/* NI (LIO) FIFO states  */
const NLF      = 0x00C0;
/* ---- ---- ---- ---- ---- -DSS EHLZ YXWV */

/* NI (LIO) FIFO entries  */
const NLE      = 0x00C2;
/* ---- ---- ---- ---- ---- ---- EEEE EEEE */

/* NI fifo errors  */
const NFE      = 0x0DC1;
/* ---- ---- ---- ---- ---- ---- ---- DCBA */

/* NI control input  */
const NCTRL    = 0x0DC0;
/* ---- ---- ---- ---- ---- ---- ---- ---C */

/* NI FSM state */
const NFSM     = 0x0DC2;
/* ---- ---- ---- ---- ---- ---- ---S SSSS */

/* NI timer 0 */
const NITM0    = 0x0A08;
/* ---- ---- ---- ---- --tt tttt tttt tttt */

/* NI timer 1 */
const NITM1    = 0x0A09;
/* ---- ---- ---- ---- --tt tttt tttt tttt */

/* NI timer 2 */
const NITM2    = 0x0A0A;
/* ---- ---- ---- ---- --tt tttt tttt tttt */

/* NI port 4 delay */
const NIP4D    = 0x0A0B;
/* ---- ---- ---- ---- --tt tttt tttt tttt */

/* GSM command register for switching clock/enable, W/O */
const SMOFFON  = 0x0A05;
/* ---- ---- dddd dddd dddd dddd dddd dddd */

/* GSM command register for switching on clock/enable, W/O */
const SMON     = 0x0A06;
/* ---- ---- ---- ---- ---- dddd dddd dddd */

/* GSM command register for switching off clock/enable, W/O */
const SMOFF    = 0x0A07;
/* ---- ---- ---- ---- ---- dddd dddd dddd */

/* NI output data port (LIO), W/O */
const NODP     = 0x0000;
/* dddd dddd dddd dddd dddd dddd dddd dddd */

/* go to low power state */
const CMD_LP       = 0x0012;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* go to acquisition state */
const CMD_ACQ      = 0x0112;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* go to test mode */
const CMD_CHK_TST  = 0x0212;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* exit clear state */
const CMD_EXT_CLR  = 0x0312;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* go to clear state */
const CMD_CLEAR    = 0x0412;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* internally generate pretrigger */
const CMD_PRETRIGG = 0x0512;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* start the self-test state machine */
const CMD_SELFTP   = 0x0612;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

/* CPU finished the tracklet processing or raw data compression */
const CMD_CPU_DONE = 0x0712;
/* ---- ---- ---- ---- ---- ---- ---- ---- */

// Interrupt numbers
const IRQ_NI  = 8;
const IRQ_TM  = 6;
const IRQ_CLR = 0;
const IRQ_ACQ = 2;
const IRQ_RAW = 4;
const IRQ_TST = 1;

// Base addresses of the programmable constants
const CPU0CONST=0x0C00; // cpu0
const CPU1CONST=0x0C08; // cpu1
const CPU2CONST=0x0C10; // cpu2
const CPU3CONST=0x0C18; // cpu3
const CPUGCONST=0x0C04; // all cpus

// Addresses of all programmable constants in the GIO space
const C08CPU0=CPU0CONST+0; // c8  of cpu0
const C09CPU0=CPU0CONST+1; // c9  of cpu0
const C10CPU0=CPU0CONST+2; // c10 of cpu0
const C11CPU0=CPU0CONST+3; // c11 of cpu0

const C08CPU1=CPU1CONST+0; // c8  of cpu1
const C09CPU1=CPU1CONST+1; // c9  of cpu1
const C10CPU1=CPU1CONST+2; // c10 of cpu1
const C11CPU1=CPU1CONST+3; // c11 of cpu1

const C08CPU2=CPU2CONST+0; // c8  of cpu2
const C09CPU2=CPU2CONST+1; // c9  of cpu2
const C10CPU2=CPU2CONST+2; // c10 of cpu2
const C11CPU2=CPU2CONST+3; // c11 of cpu2

const C08CPU3=CPU3CONST+0; // c8  of cpu3
const C09CPU3=CPU3CONST+1; // c9  of cpu3
const C10CPU3=CPU3CONST+2; // c10 of cpu3
const C11CPU3=CPU3CONST+3; // c11 of cpu3

const C12CPUA=CPUGCONST+0; // c12
const C13CPUA=CPUGCONST+1; // c13
const C14CPUA=CPUGCONST+2; // c14
const C15CPUA=CPUGCONST+3; // c15

// Global counter/timer
const CTGDINI = 0x0B80;
const CTGCTRL = 0x0B81;
const CTGDOUT = 0x0B82;
