
trapcc - The TRAP Configuration Compiler
========================================

This repository implements the `trapcc` command that is used to compile
configurations for the TRAP chips in the front-end electronics of the ALICE
TRD. `trapcc` is usually used in combination with the assembler for the MIMD
CPUs in the TRAP chip (`asm_mimd`/`codem`). `trapcc` is mostly used to set the
TRAP configurations registers, while the assembler is used to translate the
programme that will run in the front-end electronics.

`trapcc` itself does not configure the TRAP chips, it merely generates a .dat
file with commands that will be used by another programme to configure the
TRAPs. This is usually the FeeServer with the TRD Control Engine (trdce),
running on the DCS board. The .dat files are in this case stored in the wingDB,
and the Java InterCom Layer is used to extract the configuration files from the
wingDB and send them to the FeeServers. However, other programmes exist that
are mostly used for testing. It should be noted that some of the functionality
is not implemented in all programmes that use the produced .dat files, and the
implementation details may vary.

Syntax
======

Some of the syntax description is taken from the original source code. It may
be outdated, but still useful.

Include statements
------------------

A .tcs file that is processed by `trapcc` can include other .tcs files using
the `include` statement, followed by the name of the file to be included.
Simple expansion of a single variable is supported by enclosing the variable
name in curly braces. The file will be opened with fopen, using either a
relative or an absolute path.

### Examples:
For more examples, see *tests/include_main.tcs*
```
include relative/path/to/file.tcs
include /you/can/also/use/absolute/paths.tcs
NUMERIC_VARIABLE = 42
include filename_with_{NUMERIC_VARIABLE}.tcs

// for a varible STRINGDEF, defined on the the command line with -DSTRINGDEF=abc
include filename_with_{STRINGDEF}.tcs
```

Variables
---------

Variables can hold integer values and come in various flavours:

  - **regular** variables can be redefined any number of times
  - **definitions** with `-D` on the command line can be used to 'preload'
    variables
  - **const**: must be initialized when declared and are immutable afterwards
  - **asm**: like **const** variables, but will also be written to the
    assembler file if requested.

### Variable assignment

The `=` operator can be used to initialize variables and constants, and to
assign new values to variables and command-line definitions. In addition, the
operator `?=` (inspired by GNU Make), can be used to initialize or set variables
or constants if they have not been set previously. The main use for this is to
provide sensible defaults in a .tcs file and allow overriding this default with
a command line switch. This feature was implemented to simplify the generation
of different configurations from the same source file in TRAPconfig.


### Examples:
For more examples, see *tests/variables.tcs*
```
const one=1
foo = 3
asm bar = foo
```

Logging and error handling
--------------------------

The statements `error`, `warning`, `echo` and `printf` can be used to send
output to stdout, corresponding to loglevels *error*, *warning* and
*info* (for `printf` and `echo`). A *debug* loglevel is defined, but currently
not used. By default, warning messages and above are logged, and this can be
changed with the command line switches `-q` and `-v`. `printf` and `error`
support printf-style string formatting with up to two integer arguments.

### Examples:
```
error 1 "This errror will always trigger and terminate the program"
warning a==1 "This warning will show if a is 1"
echo "The answer is %d" 42
printf "Even when we use printf, the answer is still 0x%X" 42
```

Statements
----------

| Statement | Description |
| --------- | ----------- |
| <code>include *filename.tcs*</code> | include other script file (can be nested) |
| <code> assemble filename.asm        </code> | process asm file, include assembler output here |
| <code> const theConst = 6           </code> | define constant (case-sensitive) |
| <code> asm theAsmConst = 7          </code> | define constant (case-sensitive) and write to assembler file |
| <code> theOther = theConst + 1      </code> | define variable (case-sensitive) |
| <code> write *id*, *addr*, *val*    </code> | generate scsn write command |
| <code> write *addr*, *val*          </code> | generate scsn write command (broadcast) |
| <code> read *id*, *addr*            </code> | generate scsn read command |
| <code> expect *id*, *addr*, *val*   </code> | generate hinted scsn read command |
| <code> readseq *id*, *addr*, *cnt*  </code> | generate a sequence of scsn read commands |
| <code> reset *id*                   </code> | generate scsn reset command |
| <code> reset                        </code> | generate scsn reset command (broadcast) |
| <code> nop                          </code> | generate scsn nop/ping command |
| <code> bridge *id*, *val*           </code> | generate scsn bridge command |
| <code> pretrigger *val*             </code> | generate testbench pretrigger command |
| <code> wait *time*                  </code> | generate testbench wait command |
| <code> wait *times*, *state*, *slv* </code> | check up to times for slv going into state |
| <code> restrict *exp*               </code> | only process following commands if <code>*exp*</code> is true |

Integer expressions
-------------------

| Type            | Supported operators           |
| --------------- | ----------------------------- |
| number formats: | `386` (decimal), `0xFFFF` (hex), `1010b` (binary), `755o` (octal) |
| assignment:     | `=`                           |
| arithmetic:     | `+` `-` `*` `/` `%`           |
| binary:         | `\|` `^` `&` `~` `<<` `>>`    |
| parentheses:    | `( )`                         |
| comparison:     | `==` `!=` `<` `<=` `>` `>=`   |
| logical:        | `\|\|` `&&` `!`               |

General
-------

Trapcc understands `/* c-style */` and `// c++-style` comments. Statements are separated by semicolons(`;`) or newlines (`\n`).

History
=======

Original README by Jan de Cuveland
----------------------------------
```
/*
 * TCC - TRD Configuration Compiler
 * Author: Jan de Cuveland <cuveland@kip.uni-heidelberg.de>
 *         Kirchhoff-Institut fuer Physik
 * Date: December 17, 2003
 */

Hello TRAP group,

To simplify the configuration of TRAP for both simulation and testing,
I wrote a small tool which generates a chain of slow control commands
from a configuration script file.

The program can perform arithmetic operations, understand several
number representations and handle constants and variables.

The full set of configuration register names from the TRAP
documentation is available as preloaded constants. These constants
should be used instead of explicit address values whereever possible.

As the asm_mimd assembler can be executed from within a configuration
script, it is now possible to have only a single configuration source
file for each simulation.

The program is available on tieda as "tcc" (type "tcc --help" for more
information).  It is ment to replace "inc2dat" and any direct calls to
"asm_mimd" or "m4".

A migration script named "inc2tcs" (also available on tieda) tries to
convert existing scsn data files to the new .tcs format used by
tcc. Use it as "inc2tcs < rusty.inc > shiny.tcs". I will remove this
script from tieda after some time.

Regards,

  Jan.
```

Extensions by Tom Dietel
------------------------
(ca. 2006-2013)

I made several updates to tcc over the last few years:

I included some code that should determine proper locations of error
messages (i.e. file/line instead of total number of lines read so
far). However, this is mostly untested.

Second, I renamed tcc to trapcc, to avoid a naming conflict with the
tiny c compiler, which has lead to confusion several times.

Last, I introduced the first two extended commands (command IDs >= 16)
that are not part of the original tcc. So far, tracklet/LTU parameters
can be set by trapcc (although some definitions are still pending) and
the tests can be started.

To extend the trapcc by more extended commands, the code has to be
modified in three places: first the tokens that should be used have to
be listed in parser.y on a %token line. Second, lexer.l has to be
modified to generate the new tokens when the new commands are
encountered, and third, the grammar in parser.y has to be modified to
allow for the new tokens and to generate the correct output.
