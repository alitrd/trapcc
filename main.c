/*
 * TCC - TRD Configuration Compiler
 * Author: Jan de Cuveland <cuveland@kip.uni-heidelberg.de>
 *         Kirchhoff-Institut fuer Physik
 * Date: December 15, 2003
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include "tcc.h"
#include "parser.h"
#include "config.h"
//#include "tcc.tab.h"


int yyparse();
void yyrestart(FILE *);

extern FILE *yyin;
extern char *current_filename;
extern int   yylineno;

int word_count = 0;
int active = 1;
char *afile = 0;

FILE* datfile = NULL;
FILE* depfile = NULL;
char datfilename[256];

/*************************************************************************/
/* The symbol table: a chain of `struct symrec'.                         */
/*************************************************************************/

/* Entry point to the symbol table */
symrec *sym_table;

/* Add a symbol to the symbol table */
symrec * putsym(char const *sym_name, int sym_type)
{
  symrec *ptr;
  ptr = (symrec *) malloc(sizeof(symrec));
  ptr->name = (char *) malloc(strlen(sym_name) + 1);
  strcpy(ptr->name, sym_name);
  ptr->type = sym_type;
  ptr->flags = 0;
  ptr->value.var = 0; /* Set value to 0 even if fctn.  */
  ptr->next = (struct symrec *) sym_table;
  sym_table = ptr;
  return ptr;
}

/* Retrieve a symbol from the symbol table */
symrec * getsym(char const *sym_name)
{
  symrec *ptr;
  for (ptr = sym_table; ptr != (symrec *) 0; ptr = (symrec *) ptr->next)
    if (strcmp(ptr->name, sym_name) == 0)
      return ptr;
  return 0;
}


/*************************************************************************/
/* Output                                                                */
/*************************************************************************/

loglevel_t loglevel = eWARN;

void message(loglevel_t lvl, char* msg, ...)
{
  va_list args;

  if (!active) return;
  if (lvl < loglevel) return;

  switch(lvl) {
  case eDEBUG: printf("[D] "); break;
  case eINFO:  printf("[I] "); break;
  case eWARN:  printf("[W] "); break;
  case eERROR: printf("[E] "); break;
  default:     printf("[?] "); break;
  }

  va_start(args, msg);
  vprintf(msg, args);
  va_end(args);

  printf("\n");

  if (lvl==eERROR) exit(-1);
}

void
write_word(int cmd, int id, int addr, int val)
{
  if (active) {
    if (!afile && datfile)
      fprintf(datfile, "%d %d %d %d %d\n", cmd, addr, val, id, word_count);
    word_count++;
  }
}


#define BUFSIZE 1024

void
assemble_file(int id, char *fn)
{
  char ch[BUFSIZE];
  FILE *fp;

  /* call m4 */
  if (snprintf(ch, BUFSIZE,
               "m4 - %s %s > %s_tmp <<-EOF\n"
               "changecom(\\`;', \\`\n')dnl\EOF\n",
               M4INC, fn, fn) >= BUFSIZE) {
    fprintf(stderr, "ERROR: filename too long\n");
    exit(1);
  }
  if (system(ch) != 0) {
    fprintf(stderr, "ERROR: error executing m4\n");
    exit(1);
  }
  /* call asm_mimd */
  if (snprintf(ch, BUFSIZE,
               "asm_mimd %s_tmp %s_tmp.dummy %s"
               " -ve -ohDL -S%d",
               fn, fn, fn, id) >= BUFSIZE) {
    fprintf(stderr, "ERROR: filename too long\n");
    exit(1);
  }
  if (system(ch) != 0) {
    fprintf(stderr, "ERROR: error executing asm_mimd\n");
    exit(1);
  }
  /* remove file 'something.asm_tmp' */
  if (snprintf(ch, BUFSIZE, "%s_tmp", fn) >= BUFSIZE) {
    fprintf(stderr, "ERROR: filename too long\n");
    exit(1);
  }
  if (unlink(ch) != 0) {
    fprintf(stderr, "ERROR: cannot remove file '%s'\n", ch);
    exit(1);
  }
  /* print file 'something.asm_tmp.dat' */
  if (snprintf(ch, BUFSIZE, "%s_tmp.dat", fn) >= BUFSIZE) {
    fprintf(stderr, "ERROR: filename too long\n");
    exit(1);
  }
  if (!(fp = fopen(ch, "r"))) {
    fprintf(stderr, "ERROR: cannot read file '%s'\n", ch);
    exit(1);
  }
  while (!feof(fp)) {
    fgets(ch, BUFSIZE, fp);
    if (ch[0] != '\0')
      ch[strlen(ch)-1] = '\0';
    if (!feof(fp)) {
      if (!afile) printf("%s %d\n", ch, word_count);
      word_count++;
    }
  }
  fclose(fp);
  /* remove file 'something.asm_tmp.dat' */
  if (snprintf(ch, BUFSIZE, "%s_tmp.dat", fn) >= BUFSIZE) {
    fprintf(stderr, "ERROR: filename too long\n");
    exit(1);
  }
  if (unlink(ch) != 0) {
    fprintf(stderr, "ERROR: cannot remove file '%s'\n", ch);
    exit(1);
  }
}

void
print_help(void)
{
  printf("tcc - TRD Configuration Compiler\n"

         "\nUSAGE\n"
         "  tcc script-file.tcs ... > sc_send.dat\n"

         "\nOPTIONS\n"
         "  -V, --version                display version information.\n"
         "  -h, --help                   display this option summary.\n"
         "  -q, --quiet                  decrease log level.\n"
         "  -v, --verbose                increase log level.\n"
         "  -c <config_file>             use alternative config_file.\n"
         "  -o <dat_file>                output file for SCSN/dat commands.\n"
         "  -s <asm_file>                output file for assembler constants.\n"
         "  -d <deps_file>               dependencies file for ninja.\n"

         "\nSYNTAX DESCRIPTION\n"
         "\nSee README.md file.\n\n");
}

void
print_version(void)
{
  printf("%s, version %s\n", PACKAGE_NAME, PACKAGE_VERSION);
}

int
main(int argc, char **argv)
{
  char std_inc_path[1000];
  strcpy(std_inc_path, STDINC);
  char* std_inc;

   // = STDINC;
  int index;
  int option_index = 0;
  int c;
  FILE *fp;
  FILE *asmfile = NULL;
  // char asmappend[1024];
  char tmpstr[1024];

  /* variables for command line defines */
  char *defname;
  char *defvalue;
  int defnumber;
  symrec *defsym;

  static struct option long_options[] =
    { {"help",    no_argument, 0, 'h'},
      {"version", no_argument, 0, 'V'},
      {"verbose", no_argument, 0, 'v'},
      {"quiet", no_argument, 0, 'q'},
      {"annotate", required_argument, 0, 'a'},
      {"output", required_argument, 0, 'o'},
      {"assembler-defs", required_argument, 0, 's'},
      // {"assembler-append", required_argument, 0, 'A'},
      {"dependencies-file", required_argument, 0, 'd'},
      {"define", required_argument, 0, 'D'},
      {0, 0, 0, 0} };

  while ((c = getopt_long(argc, argv, "hVa:c:s:o:d:D:vq", long_options,
                          &option_index)) != -1)
    switch (c) {

      /* show help/version and exit */
      case 'h': print_help(); return 0;
      case 'V': print_version(); return 0;

      /* decrease / increase loglevel */
      case 'v': if (loglevel > eDEBUG) loglevel--; break;
      case 'q': if (loglevel < eERROR) loglevel++; break;

      case 'a':
        afile = strdup(optarg);
        break;
      case 'o':
        datfile = fopen(optarg,"w");
        strncpy(datfilename,optarg,256);
        datfilename[255]='\0';
        break;
      case 's':
        asmfile = fopen(optarg,"w");
        break;
      case 'd':
        depfile = fopen(optarg,"w");
        break;
      case 'c':
        strcpy(std_inc,strdup(optarg));
        break;

      case 'D':
        strncpy(tmpstr, optarg, 1024);
        defname = strtok(tmpstr,"=");
        defvalue = strtok(NULL,"=");
        defnumber = strtol(defvalue, NULL, 0);
        if (defnumber==0 && errno==EINVAL) {
          defsym = putsym(defname,STRDEF);
          defsym->value.str = strdup(defvalue);
          message(eDEBUG, "def string: %s=%s", defsym->name, defsym->value.str);
        } else {
          defsym = putsym(defname,DEF);
          defsym->value.var = defnumber;
          message(eDEBUG, "def: %s=%d", defsym->name, defsym->value.var);
        }
        break;

      case '?':
        /* getopt_long already printed an error message. */
        return 1;
      default:
        abort();
      }

  if ( datfile && depfile ) {
    fprintf(depfile, "%s:", datfilename);
  }

  if ( !datfile && !asmfile ) {
    datfile = stdout;
  }

  // loop over std include path to find config file
  for(std_inc=strtok(std_inc_path, ":"); std_inc!=NULL; std_inc=strtok(NULL,":")) {

    // message(eDEBUG, "trying to read std include file \"%s\"", std_inc);
    yyin = fopen(std_inc, "r");

    if (yyin) {
      current_filename = std_inc;
      if (depfile) {
        fprintf(depfile, " %s", current_filename);
      }
      message(eDEBUG, "include \"%s\"", current_filename);

      break;
    }

  }

  if (!yyin ) {
    fprintf(stderr, "ERROR: cannot open std include file\n");
    // fprintf(stderr, "ERROR: cannot open std include file '%s'\n", std_inc);
    exit(1);
  }

  // current_filename = std_inc;
  yyparse();

  /* process files */
  for (index = optind; index < argc; index++) {
    fp = fopen(argv[index], "r");
    if (fp != NULL) {
      current_filename = argv[index];
      if (depfile) {
        fprintf(depfile, " %s", current_filename);
      }
      yylineno = 1;
      yyin = fp;
      yyrestart(yyin);
      yyparse();
    } else {
      printf("ERROR: cannot open file '%s'\n", argv[index]);
      return 1;
    }
  }

  /* process stdin */
  if (optind == argc) {
    yyin = stdin;
    yyrestart(yyin);
    yyparse();
  }

  if (asmfile) {
    symrec *sym;
    for (sym = sym_table; sym != (symrec *) 0; sym = (symrec *) sym->next) {
      if ( sym->flags & flASM ) {
        fprintf(asmfile, "#def %-24s \t= 0x%08x;\n", sym->name, sym->value.var);
      }
    }
  }

  if (depfile) {
    fprintf(depfile, "\n");
  }
  return 0;
}
