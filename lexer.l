/*
 * TCC - TRD Configuration Compiler
 * Author: Jan de Cuveland <cuveland@kip.uni-heidelberg.de>
 *         Kirchhoff-Institut fuer Physik
 * Extended by:
 *         Tom Dietel <tom@dietel.net>
 *         IKP Muenster
 */

%option yylineno
%option noyywrap

%{
#include "tcc.h"
#include "parser.h"
#include <string.h>

  int yyparse();
#if defined (__GNUC_MINOR__) && 2093 <= (__GNUC__ * 1000 + __GNUC_MINOR__)
  static void __attribute__ ((__unused__)) yyunput();
#endif
  char *current_filename;
  symrec *s;
  size_t txtlen;

#define MAX_INCLUDE_DEPTH 100

  struct {
      char *           filename;
      YY_BUFFER_STATE  buffer;
      int              lineno;
  } inc_stack[MAX_INCLUDE_DEPTH];
  int inc_depth = 0;

%}

DIGIT    [0-9]
ID       [a-zA-Z][a-zA-Z0-9_]*

%x comment incl asm

%%

"/*"        BEGIN(comment);

<comment>[^*\n]*        /* eat anything that's not a '*' */
<comment>"*"+[^*/\n]*   /* eat up '*'s not followed by '/'s */
<comment>\n             /* ++line_num; */
<comment>"*"+"/"        BEGIN(INITIAL);

"//".*\n    /* strip c++-style comment */
"#".*\n     /* strip shell-style comment */

include     BEGIN(incl);

<incl>[ \t]*     /* eat the whitespace */
<incl>[^ \t\n\015]+  {

   if (active) {

      /* Save current state on stack */
      inc_stack[inc_depth].filename = current_filename;
      inc_stack[inc_depth].buffer   = YY_CURRENT_BUFFER;
      /* inc_stack[inc_depth].input    = yyin; */
      inc_stack[inc_depth].lineno   = yylineno;
      inc_depth++;

      /* Replace occurences of {SYMNAME} */
      /* char pre[100], varname[100], post[100]; */
      char *pre, *varname, *post;

      pre = strsep(&yytext, "{");
      varname = strsep(&yytext, "}");
      post = strsep(&yytext, "}");

      if (varname && post) {
        s = getsym(varname);
        if (!s) {
          fprintf(stderr, "ERROR: invalid include statement"
                          " on %s:%d\n", current_filename, yylineno);
          exit(-1);
        }

        switch(s->type) {
          case VAR:
          case DEF:
          case CONST:
            txtlen = strlen(pre) + strlen(post) + 10;
            current_filename = (char*)malloc(txtlen + 1);
            sprintf( current_filename, "%s%d%s", pre, s->value.var, post);
            break;

          case STRDEF:
            txtlen = strlen(pre) + strlen(post) + strlen(s->value.str);
            current_filename = (char*)malloc(txtlen + 1);
            sprintf(current_filename, "%s%s%s", pre, s->value.str, post);
            break;

          default:
            fprintf(stderr, "ERROR: invalid include statement"
                            " on %s:%d\n", current_filename, yylineno);
             exit(1);
        }
      } else {
        current_filename = strdup(pre);
      }

      message(eDEBUG, "including %s", current_filename);
      yyin = fopen(current_filename, "r");
      if (!yyin) {
        fprintf(stderr, "ERROR: cannot open tcs-include file '%s'"
                        " on line %d\n", current_filename, yylineno);
        exit(1);
      }

      if (depfile) {
        fprintf(depfile, " %s", current_filename);
      }
      yy_switch_to_buffer(yy_create_buffer(yyin, YY_BUF_SIZE));

      yylineno=1;

   }
   BEGIN(INITIAL);
}

<<EOF>>     {

   if ( inc_depth == 0 ) {

        yyterminate();

    } else {

        inc_depth--;

        yy_delete_buffer( YY_CURRENT_BUFFER );
        yy_switch_to_buffer(inc_stack[inc_depth].buffer);
        free(current_filename);

        yylineno = inc_stack[inc_depth].lineno;
        current_filename = inc_stack[inc_depth].filename;

    }

}

assemble    BEGIN(asm);

<asm>[ \t]*      /* eat the whitespace */
<asm>[^ \t\n\015]+   { if (active) assemble_file(127, yytext);
                   BEGIN(INITIAL); }

{DIGIT}+    { sscanf(yytext, "%d", &yylval.val);
              return NUM; }

0[Xx][0-9A-Fa-f]+ { sscanf(yytext, "%x", &yylval.val);
              return NUM; }

[01]+b      { yylval.val = 0;
              while (*yytext != 'b')
		yylval.val = yylval.val * 2 + (*yytext++ - '0');
	      return NUM; }

[0-7]+o     { sscanf(yytext, "%o", &yylval.val);
              return NUM; }

[0-9]+\.[0-9]+    { sscanf(yytext, "%f", &yylval.fval);
                   return FLOAT; }

\"[^\"]*\"        { /* handle simple strings */
                    int len = strlen(yytext);
                    yylval.str = (char*)malloc(len);
                    strncpy(yylval.str, yytext+1, len-2);
                    /* printf("FOUND STRING: >>%s<<", yytext); */
                    return STRING; }

warning     { return WARNING; }
error       { return ERROR; }
echo        { return PRINT; }
printf      { return PRINT; }

const       { return CONSTVAR; }
asm         { return ASMVAR; }
send        { return WRITE; }
write       { return WRITE; }
reset       { return RESET; }
nop	        { return NOP; }
read	      { return READ; }
expect	    { return EXPECT; }
readseq     { return READSEQ; }
pretrigger  { return PRETRIGGER; }
wait        { return WAIT; }
bridge      { return BRIDGE; }

all         { return ALL; }
default     { return DEFAULT; }
loglevel    { return LOGLEVEL; }
get         { return GET; }
run         { return RUN; }

restrict    { return RESTRICT; }
sector      { return SECTOR; }
stack       { return STACK; }
layer       { return LAYER; }
rocs        { return ROCS; }

tracklet    { return TRACKLET; }
minpt       { return MINPT; }
bfield      { return BFIELD; }
omegatau    { return OMEGATAU; }
driftbins   { return DRIFTBINS; }
apply       { return APPLY; }

scaleq0     { return SCALEQ0; }
scaleq1     { return SCALEQ1; }
tracklengthcorr { return TRACKLENGTHCORR; }
tiltedcorr  { return TILTEDCORR; }

test        { return CETEST; }
ni          { return NI; }
ori         { return ORI; }
shutdown    { return SHUTDOWN; }
laserid     { return LASERID; }
nifast      { return NIFAST; }
dmm         { return DMM; }
ddd         { return DDD; }
imm         { return IMM; }
nisingle    { return NISINGLE; }
ping        { return PING; }

hcid        { return HCID; }

rstate      { return RSTATE; }
gsmstate    { return GSMSTATE; }
nistate     { return NISTATE; }
evcnt       { return EVCNT; }
ptrgcnt     { return PTRGCNT; }
lcnt0       { return LC0CNT; }
lcnt1       { return LC1CNT; }
lcnt2       { return LC2CNT; }
lcnt3       { return LC3CNT; }

ecrit       { return ECRITICAL; }
eerror      { return EERROR; }
ewarn       { return EWARN; }
einfo       { return EINFO; }
edebug      { return EDEBUG; }

"?="        { return ASSIGNX; }

"=="        { return EQ; }
"!="        { return NE; }
"<"         { return LT; }
"<="        { return LE; }
">"         { return GT; }
">="        { return GE; }

"||"        { return OR; }
"&&"        { return AND; }
"<<"        { return SHL; }
">>"        { return SHR; }
"++"        { return INC; }
"--"        { return DEC; }

{ID}        { s = getsym(yytext);
              if (s == 0)
		s = putsym(yytext, UNSET);
	      yylval.tptr = s;
	      return s->type; }

[ \t]*      { ; }

.|\n        { return yytext[0]; }

%%
